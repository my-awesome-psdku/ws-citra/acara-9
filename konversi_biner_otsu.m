function varargout = konversi_biner_otsu(varargin)
% KONVERSI_BINER_OTSU MATLAB code for konversi_biner_otsu.fig
%      KONVERSI_BINER_OTSU, by itself, creates a new KONVERSI_BINER_OTSU or raises the existing
%      singleton*.
%
%      H = KONVERSI_BINER_OTSU returns the handle to a new KONVERSI_BINER_OTSU or the handle to
%      the existing singleton*.
%
%      KONVERSI_BINER_OTSU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KONVERSI_BINER_OTSU.M with the given input arguments.
%
%      KONVERSI_BINER_OTSU('Property','Value',...) creates a new KONVERSI_BINER_OTSU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before konversi_biner_otsu_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to konversi_biner_otsu_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help konversi_biner_otsu

% Last Modified by GUIDE v2.5 20-Sep-2023 09:31:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @konversi_biner_otsu_OpeningFcn, ...
                   'gui_OutputFcn',  @konversi_biner_otsu_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before konversi_biner_otsu is made visible.
function konversi_biner_otsu_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to konversi_biner_otsu (see VARARGIN)

% Choose default command line output for konversi_biner_otsu
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes konversi_biner_otsu wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = konversi_biner_otsu_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
[name_file1,name_path1] = uigetfile( ...
 {'*.bmp;*.jpg;*.tif','Files of type (*.bmp,*.jpg,*.tif)';
 '*.bmp','File Bitmap (*.bmp)';...
 '*.jpg','File jpeg (*.jpg)';
 '*.tif','File Tif (*.tif)';
 '*.*','All Files (*.*)'},...
 'Open Image');
if ~isequal(name_file1,0)
 handles.data1 = imread(fullfile(name_path1,name_file1));
 guidata(hObject,handles);
 axes(handles.axes1);
 imshow(handles.data1);
else
 return;
end
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
image1 = handles.data1;
gray = rgb2gray(image1);
axes(handles.axes2);
imshow(gray);
handles.data2 = gray;
guidata(hObject,handles);
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
thresh = handles.data3;
[name_file_save,path_save] = uiputfile( ...
 {'*.bmp','File Bitmap (*.bmp)';...
 '*.jpg','File jpeg (*.jpg)';
 '*.tif','File Tif (*.tif)';
 '*.*','All Files (*.*)'},...
 'Save Image');
if ~isequal(name_file_save,0)
 imwrite(thresh,fullfile(path_save,name_file_save));
else
 return
end

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
gray = handles.data2;
value = graythresh(gray);
thresh = im2bw(gray,value);
thresh = imcomplement(thresh);
axes(handles.axes2);
imshow(thresh);
handles.data3 = thresh;
guidata(hObject,handles);
