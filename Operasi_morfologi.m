function varargout = Operasi_morfologi(varargin)
% OPERASI_MORFOLOGI MATLAB code for Operasi_morfologi.fig
%      OPERASI_MORFOLOGI, by itself, creates a new OPERASI_MORFOLOGI or raises the existing
%      singleton*.
%
%      H = OPERASI_MORFOLOGI returns the handle to a new OPERASI_MORFOLOGI or the handle to
%      the existing singleton*.
%
%      OPERASI_MORFOLOGI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPERASI_MORFOLOGI.M with the given input arguments.
%
%      OPERASI_MORFOLOGI('Property','Value',...) creates a new OPERASI_MORFOLOGI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Operasi_morfologi_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Operasi_morfologi_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Operasi_morfologi

% Last Modified by GUIDE v2.5 20-Sep-2023 10:47:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Operasi_morfologi_OpeningFcn, ...
                   'gui_OutputFcn',  @Operasi_morfologi_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Operasi_morfologi is made visible.
function Operasi_morfologi_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Operasi_morfologi (see VARARGIN)

% Choose default command line output for Operasi_morfologi
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Operasi_morfologi wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Operasi_morfologi_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
[filename,pathname] = uigetfile({'*.*'});
if ~isequal(filename,0)
 Info = imfinfo(fullfile(pathname,filename));
 if Info.BitDepth == 1
 msgbox('Citra masukan harus citra RGB atau Grayscale');
 return
 elseif Info.BitDepth == 8
 Img_Gray = imread(fullfile(pathname,filename));
 axes(handles.axes1)
 cla('reset')
 imshow(Img_Gray)
 title('Grayscale Image')
 Img_bw = im2bw(Img_Gray,graythresh(Img_Gray));
 axes(handles.axes2)
 cla('reset')
 imshow(Img_bw)
 title('Binary Image')
 else
 Img_Gray = rgb2gray(imread(fullfile(pathname,filename)));
 axes(handles.axes1)
 cla('reset')
 imshow(Img_Gray)
 title('Grayscale Image')
 Img_bw = im2bw(Img_Gray,graythresh(Img_Gray));
 axes(handles.axes2)
 cla('reset')
 imshow(Img_bw)
 title('Binary Image')
 end
else
 return
end 
handles.Img_Gray = Img_Gray;
handles.Img_Gray2 = Img_Gray;
handles.Img_bw = Img_bw;
handles.Img_bw2 = Img_bw;
guidata(hObject,handles); 
set(handles.pushbutton2,'Enable','on')


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
Img_Gray = handles.Img_Gray;
Img_bw = handles.Img_bw;
% Operations
val1 = get(handles.radiobutton1,'Value');
val2 = get(handles.radiobutton2,'Value');
% Structure Element
val5 = get(handles.radiobutton3,'Value');
val6 = get(handles.radiobutton4,'Value');
val7 = get(handles.radiobutton5,'Value');
val8 = get(handles.radiobutton6,'Value');
% R
R = str2double(get(handles.edit1,'String'));
%
if val5 == 1
 se = strel('disk',R);
elseif val6 == 1
 se = strel('diamond',R);
elseif val7 == 1
 se = strel('square',R);
elseif val8 == 1
 se = strel('line',R,45);
end
if val1 == 1
 Morph_Gray = imerode(Img_Gray,se);
 Morph_bw = imerode(Img_bw,se);
elseif val2 == 1
 Morph_Gray = imdilate(Img_Gray,se);
 Morph_bw = imdilate(Img_bw,se);
end
axes(handles.axes1)
cla('reset')
imshow(Img_Gray)
title('Grayscale Image')
axes(handles.axes2)
cla('reset')
imshow(Img_bw)
title('Binary Image')
axes(handles.axes3)
cla('reset')
imshow(Morph_Gray)
title('Morphological Operation')
axes(handles.axes4)
cla('reset')
imshow(Morph_bw)
title('Morphological Operation')

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)

set(gca,'YTick',[])
axes(handles.axes2)
cla('reset')
set(gca,'XTick',[])
set(gca,'YTick',[])
axes(handles.axes3)
cla('reset')
set(gca,'XTick',[])
set(gca,'YTick',[])
axes(handles.axes4)
cla('reset')
set(gca,'XTick',[])
set(gca,'YTick',[])
set(handles.pushbutton2,'Enable','off')
set(handles.edit1,'String',1)
set(handles.radiobutton1,'Value',1)
set(handles.radiobutton2,'Value',0)
set(handles.radiobutton3,'Value',1)
set(handles.radiobutton4,'Value',0)
set(handles.radiobutton5,'Value',0)
set(handles.radiobutton6,'Value',0)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
val = str2double(get(handles.edit1,'String'))-1;
if val < 1
 val = 1;
end
set(handles.edit1,'String',val)

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
val = str2double(get(handles.edit1,'String'))+1;
if val > 10
 val = 10;
end
set(handles.edit1,'String',val)

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
set(handles.radiobutton1,'Value',1) %untuk erode(erosi)
set(handles.radiobutton2,'Value',0) %untuk dilate(dilatasi)


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
set(handles.radiobutton1,'Value',0) %untuk erode(erosi)
set(handles.radiobutton2,'Value',1) %untuk dilate(dilatasi)


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
set(handles.radiobutton3,'Value',1) %Untuk Strel disk
set(handles.radiobutton4,'Value',0) %Untuk Strel diamond
set(handles.radiobutton5,'Value',0) %Untuk Strel square
set(handles.radiobutton6,'Value',0) %Untuk Strel line

% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
set(handles.radiobutton3,'Value',0) %Untuk Strel disk
set(handles.radiobutton4,'Value',1) %Untuk Strel diamond
set(handles.radiobutton5,'Value',0) %Untuk Strel square
set(handles.radiobutton6,'Value',0) %Untuk Strel line

% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
set(handles.radiobutton3,'Value',0) %Untuk Strel disk
set(handles.radiobutton4,'Value',0) %Untuk Strel diamond
set(handles.radiobutton5,'Value',1) %Untuk Strel square
set(handles.radiobutton6,'Value',0) %Untuk Strel line

% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
set(handles.radiobutton3,'Value',0) %Untuk Strel disk
set(handles.radiobutton4,'Value',0) %Untuk Strel diamond
set(handles.radiobutton5,'Value',0) %Untuk Strel square
set(handles.radiobutton6,'Value',1) %Untuk Strel line
